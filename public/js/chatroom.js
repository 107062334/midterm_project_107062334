window.onload = function()
{   
    this.Notification.requestPermission(function(status){
        if (this.Notification.permission !== status)
            this.Notification.permission = status;
    })
    
    $("#nav_bar").show(500);
    $("#main").show(500);

    var user_email;
    var user_name;
    var current_room_ref;
    var associated_rooms = [];

    firebase.auth().onAuthStateChanged(function(user){
        if (user)
        {
            $("#notification_word").html("Login success");
            $("#notification").toast("show");
            $("#nav_bar_account").html(user.email);
            user_email = user.email;
            firebase.database().ref("users").orderByChild("email").equalTo(user_email).once("value",function(snapshot){
                    snapshot.forEach(function(childsnapshot){
                        user_name = childsnapshot.ref.key;
                })
            })
        }
        else
        {
            window.location.replace("index.html");
        }
        
    })

    
    $("#room_btn").click(function(){
        $("#main").show(500);
        $("#chatroom").hide(500);
        $("#p_chatroom").hide(500);
        setTimeout(function(){$("#text_part").html("");$("#p_text_part").html("");},500);
        firebase.database().ref("rooms/"+current_room_ref+'/text').off();
        firebase.database().ref('public'+'/text').off();
        current_room_ref = "";
    })

    $("#p_room_btn").click(function(){
        if (current_room_ref!='public')
        {
            $("#main").hide(500);
            $("#chatroom").hide(500);
            $("#p_chatroom").show(500);
            firebase.database().ref("rooms/"+current_room_ref+'/text').off();
            current_room_ref = 'public';
            firebase.database().ref('public/text').on("child_added",function(ssnapshot)
            {   
                if (ssnapshot.val().sender != user_email)
                {
                    $("#p_text_part").append('<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false" id='+ssnapshot.key+'><div class="toast-header"><img src="icons/message_icon.png" class="rounded mr-2"></img><strong class="mr-auto"></strong></div><div class="toast-body" style="word-wrap:break-word;"></div></div>');
                }
                else
                {
                    $("#p_text_part").append('<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false" style="text-align:right;" id='+ssnapshot.key+'><div class="toast-header" style="justify-content:flex-end;"><img src="icons/message_icon.png" class="rounded mr-2"></img><strong class="mr-auto" style="margin-right:0 !important"></strong><button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="toast-body" style="word-wrap:break-word;"></div></div>');
                }
                $("#"+ssnapshot.key+' strong').text(ssnapshot.val().username);
                $("#"+ssnapshot.key+' .toast-body').text(ssnapshot.val().text);
                $("#"+ssnapshot.key+' button').click(function(){
                    ssnapshot.ref.remove();
                    setTimeout(()=>{$("#"+ssnapshot.key).remove()},500);
                })
                $("#p_text_part .toast").toast("show");
                p_text_part.scrollTop = p_text_part.scrollHeight;
            })
            firebase.database().ref("public/text").on("child_removed",function(ssnapshot)
            {   
                if (ssnapshot.val().sender!=user_email)
                    $("#"+ssnapshot.key).remove();
            })
        }
    })

    $("#logout_btn").click(function(){
        firebase.auth().signOut().then(function(){
            $("#text_part").html("");
            $("#p_text_part").html("");
            firebase.database().ref("rooms/"+current_room_ref+'/text').off();
            firebase.database().ref('public'+'/text').off();
            window.location.replace("index.html");
        }).catch(function(error){
            alert(error.message);
        })
        $("#text_part").html("");
    })

    

    $("#add_room_btn").click(function(){
        var chatroom_name = $("#add_room_input").val();
        if (chatroom_name != "")
        {
            firebase.database().ref("rooms/").push({
                name:chatroom_name,
                establisher:user_email,
                receiver:"",
                timestamp:+ new Date()
            })
            $("#notification_word").html("Room "+ chatroom_name +" added");
            $("#notification").toast("show");
        }
        else
        {
            $("#notification_word").html("Room name can not be empty");
            $("#notification img").attr("src","icons/wrong_icon.png");
            $("#notification").toast("show");
            setTimeout(()=>{$("#notification img").attr("src","icons/done_icon.png");},1200);
            
        } 
    })

    firebase.database().ref("rooms/").on("child_added",function(snapshot){
        snapshot.child("text").ref.orderByKey().startAt((+ new Date()).toString()).on("child_added",function(text_data){
            if (snapshot.val().establisher == user_email && text_data.val().sender != user_email && Notification && Notification.permission == "granted")
            {   
                var notif = new Notification("New message from " + text_data.val().username + " in Room '" + snapshot.val().name + "'" , {body:text_data.val().text});
                setTimeout(function(){notif.close()},2000);
            }
        })
    })

    firebase.database().ref("rooms/").on("child_added",function(snapshot){
        snapshot.child("text").ref.orderByKey().startAt((+ new Date()).toString()).on("child_added",function(text_data){
            if (snapshot.val().receiver == user_email && text_data.val().sender != user_email && Notification && Notification.permission == "granted")
            {   
                var notif = new Notification("New message from " + text_data.val().username + " in Room '" + snapshot.val().name + "'" , {body:text_data.val().text});
                setTimeout(function(){notif.close()},2000);
            }
        })
    })

    firebase.database().ref("public/text").orderByKey().startAt((+ new Date()).toString()).on("child_added",function(text_data){
        if (text_data.val().sender != user_email && Notification && Notification.permission == "granted")
        {   
            var notif = new Notification("New message from " + text_data.val().username + " in Public Room ", {body:text_data.val().text});
            setTimeout(function(){notif.close()},2000);
        }
    })

    firebase.database().ref("rooms/").on("child_added",function(snapshot){
        if (snapshot.val().establisher == user_email || snapshot.val().receiver == user_email)
        {   
            $("#room_list").append('<button class="btn btn-primary" id='+ snapshot.key + '>' + snapshot.val().name +'</button>');
            associated_rooms.push(snapshot.key);
            $("#"+snapshot.key).click(function(){
                $("#main").hide(500);
                $("#chatroom").show(500);
                current_room_ref = this.id;
                firebase.database().ref("rooms/"+current_room_ref).once("value",function(ssnapshot){
                    if(ssnapshot.val().receiver != "") $("#invite_input").attr("readonly","readonly")
                    else $("#invite_input").removeAttr("readonly");
                })
                firebase.database().ref("rooms/"+current_room_ref+'/text').on("child_added",function(ssnapshot)
                {   
                    if (ssnapshot.val().sender != user_email)
                    {   
                        $("#text_part").append('<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false" id='+ssnapshot.key+'><div class="toast-header"><img src="icons/message_icon.png" class="rounded mr-2"></img><strong class="mr-auto"></strong></div><div class="toast-body" style="word-wrap:break-word;"></div></div>');
                    }
                    else
                    {
                        $("#text_part").append('<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false" style="text-align:right;" id='+ssnapshot.key+'><div class="toast-header" style="justify-content:flex-end;"><img src="icons/message_icon.png" class="rounded mr-2"></img><strong class="mr-auto" style="margin-right:0 !important"></strong><button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="toast-body" style="word-wrap:break-word;"></div></div>');
                    }
                    $("#"+ssnapshot.key+' strong').text(ssnapshot.val().username);
                    $("#"+ssnapshot.key+' .toast-body').text(ssnapshot.val().text);
                    $("#"+ssnapshot.key+' button').click(function(){
                        ssnapshot.ref.remove();
                        setTimeout(()=>{$("#"+ssnapshot.key).remove()},500);
                    })
                    $("#text_part .toast").toast("show");
                    text_part.scrollTop = text_part.scrollHeight;
                }) 
                firebase.database().ref("rooms/"+current_room_ref+'/text').on("child_removed",function(ssnapshot)
                {   
                    if (ssnapshot.val().sender!=user_email)
                        $("#"+ssnapshot.key).remove();
                })
            })
        }
    })

    firebase.database().ref("rooms/").on("child_changed",function(snapshot){
        if (snapshot.val().receiver == user_email)
        {   
            if (associated_rooms.findIndex(function(key){return key==snapshot.key;}) == -1)
            {   
                $("#room_list").append('<button class="btn btn-primary" id='+ snapshot.key + '>'+ snapshot.val().name +'</button>');
                associated_rooms.push(snapshot.key);
                $("#"+snapshot.key).click(function(){
                    $("#main").hide();
                    $("#chatroom").show();
                    current_room_ref = this.id;
                    firebase.database().ref("rooms/"+current_room_ref).once("value",function(ssnapshot){
                        if(ssnapshot.val().receiver != "") $("#invite_input").attr("readonly","readonly")
                        else $("#invite_input").removeAttr("readonly");
                    })
                    firebase.database().ref("rooms/"+current_room_ref+'/text').on("child_added",function(ssnapshot)
                    {   
                        if (snapshot.val().receiver == user_email && ssnapshot.val().sender != user_email && Notification && Notification.permission == "granted")
                        {   
                            var notif = new Notification("New message from " + ssnapshot.val().username + " in Room '" + ssnapshot.val().name + "'" , {body:ssnapshot.val().text});
                            setTimeout(function(){notif.close()},2000);
                        }
                        if (ssnapshot.val().sender != user_email)
                        {
                            $("#text_part").append('<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false" id='+ssnapshot.key+'><div class="toast-header"><img src="icons/message_icon.png" class="rounded mr-2"></img><strong class="mr-auto"></strong></div><div class="toast-body" style="word-wrap:break-word;"></div></div>');
                        }
                        else
                        {
                            $("#text_part").append('<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false" style="text-align:right;" id='+ssnapshot.key+'><div class="toast-header" style="justify-content:flex-end;"><img src="icons/message_icon.png" class="rounded mr-2"></img><strong class="mr-auto" style="margin-right:0 !important"></strong><button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="toast-body" style="word-wrap:break-word;"></div></div>');
                        }
                        $("#"+ssnapshot.key+' strong').text(ssnapshot.val().username);
                        $("#"+ssnapshot.key+' .toast-body').text(ssnapshot.val().text);
                        $("#"+ssnapshot.key+' button').click(function(){
                            ssnapshot.ref.remove();
                            setTimeout(()=>{$("#"+ssnapshot.key).remove()},500);
                        })
                        $("#text_part .toast").toast("show");
                        text_part.scrollTop = text_part.scrollHeight;
                    })
                    firebase.database().ref("rooms/"+current_room_ref+'/text').on("child_removed",function(ssnapshot)
                    {   
                        if (ssnapshot.val().sender!=user_email)
                            $("#"+ssnapshot.key).remove();
                    })    
                })
            }
        }
    })

    $("#send_btn").click(function(){
        firebase.database().ref("rooms/" + current_room_ref + '/text/' + (+new Date())).set(
        {
            text:$("#text_input").val(),
            sender:user_email,
            username:user_name
        })
        $("#text_input").val("");
    })

    $("#p_send_btn").click(function(){
        firebase.database().ref("public" + '/text/' + (+new Date())).set(
        {
            text:$("#p_text_input").val(),
            sender:user_email,
            username:user_name
        })
        $("#p_text_input").val("");
    })

    $("#invite_btn").click(function(){
        if ($("#invite_input").val() == user_email)
        {
            $("#notification_word").html("You can't invite youself");
            $("#notification img").attr("src","icons/wrong_icon.png");
            $("#notification").toast("show");
            setTimeout(()=>{$("#notification img").attr("src","icons/done_icon.png");},1200);
        }
        else if ($("#invite_input").val().search(/^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/)==-1)
        {
            $("#notification_word").html("Please enter right email");
            $("#notification img").attr("src","icons/wrong_icon.png");
            $("#notification").toast("show");
            setTimeout(()=>{$("#notification img").attr("src","icons/done_icon.png");},1200);
        }
        else
        {   
            firebase.database().ref("users").orderByChild("email").equalTo($("#invite_input").val()).once("value",function(user_snapshot){
                if (user_snapshot.exists()){
                    user_snapshot.forEach(function(childsnapshot){
                        if (childsnapshot.val().email == $("#invite_input").val())
                        {  
                            firebase.database().ref("rooms/" + current_room_ref).update({receiver:$("#invite_input").val()}).then(function(){
                                $("#invite_input").val("");
                                $("#invite_input").attr("readonly","readonly");
                                $("#notification_word").html("Invite success");
                                $("#notification").toast("show");
                            }).catch(function(error){
                                $("#notification_word").html(error.message);
                                $("#notification img").attr("src","icons/wrong_icon.png");
                                $("#notification").toast("show");
                                setTimeout(()=>{$("#notification img").attr("src","icons/done_icon.png");},1200);
                            });
                            $("#invite_input").html("");
                            
                        }
                    })
                }
                else
                {
                    $("#notification_word").html("User doesn't exist");
                    $("#notification img").attr("src","icons/wrong_icon.png");
                    $("#notification").toast("show");
                    setTimeout(()=>{$("#notification img").attr("src","icons/done_icon.png");},1200);
                }
            })       
        }
    })
} 