window.onload = function()
{   
    $("#nav_bar").show(500);
    $("#group").show(500);
    var signing_up = false;
    var google_signin_user;
    var google_signin = false;
    
    $("#sign_in_btn").click(function()
    {   
        let email = $("#email_input").val();
        let password = $("#password_input").val();
        firebase.auth().signInWithEmailAndPassword(email, password).then(function() {
            window.location.replace("chatroom.html");
        }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            $("#notification_word").html(errorMessage);
            $("#notification").toast("show");
        });   
    })

    $("#sign_up_btn").click(function(){
        if (signing_up)
        {
            var _username = $("#username_input").val();
            var _email = $("#email_input").val();
            var password = $("#password_input").val();
            firebase.database().ref("users/").once("value",function(snapshot){
                if (snapshot.child(_username).exists())
                {
                    $("#notification_word").html("The username is already used.");
                    $("#notification").toast("show");
                }
                else
                {
                    firebase.auth().createUserWithEmailAndPassword(_email, password).then(function(){
                        $("#notification_word").html("Sign up success");
                        $("#notification img").attr("src","icons/done_icon.png");
                        $("#notification").toast("show");
                        setTimeout(()=>{$("#notification img").attr("src","icons/wrong_icon.png");},1200);
                        signing_up = false;
                        $("#username_input").hide();
                        $("#username_label").hide();
                        $("#cancel_sign_up_btn").hide();
                        $("#sign_in_btn").show();
                        $("#sign_in_with_google_btn").show();
                        firebase.database().ref("users/"+_username).set({
                            email:_email,
                            photoURL:""
                        })  
                    }).catch(function(error) {
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        $("#notification_word").html(errorMessage);
                        $("#notification").toast("show");
                    });
                }
            })
        }
        else if (google_signin)
        {   
            var _username = $("#username_input").val();
            firebase.database().ref("users/").once("value",function(snapshot){
                if (snapshot.child(_username).exists())
                {
                    $("#notification_word").html("The username is already used.");
                    $("#notification").toast("show");
                }
                else
                {
                    firebase.database().ref("users/"+_username).set({
                        email:google_signin_user.email,
                        uid:google_signin_user.uid,
                        photoURL:""
                    })
                    $("#username_input").val("");
                    $("#sign_up_btn").html("Sign up");
                    $("#username_input").hide();
                    $("#username_label").hide();
                    $("#email_label").show();
                    $("#password_label").show();
                    $("#email_input").show();
                    $("#sign_in_btn").show();
                    $("#password_input").show();
                    $("#sign_in_with_google_btn").show();
                    window.location.replace("chatroom.html");
                }
            })
        }
        else
        {
            signing_up = true;
            $("#username_input").css("display","inline-block");
            $("#username_label").css("display","inline-block");
            $("#cancel_sign_up_btn").show();
            $("#sign_in_btn").hide();
            $("#sign_in_with_google_btn").hide();
        }
    })

    $("#cancel_sign_up_btn").click(function(){
        if (signing_up)
        {   
            signing_up = false;
            $("#username_input").hide();
            $("#username_label").hide();
            $("#cancel_sign_up_btn").hide();
            $("#sign_in_btn").show();
            $("#sign_in_with_google_btn").show();
            $("#username_input").val('');
            $("#email_input").val('');
            $("#password_input").val('');
        }
    })

    $("#sign_in_with_google_btn").click(function(){
        let google_provider = new firebase.auth.GoogleAuthProvider();
        let flag = false;
        firebase.auth().signInWithPopup(google_provider).then(function(result) 
        {
            var token = result.credential.accessToken;
            var user = result.user;
            firebase.database().ref("users/").orderByChild("email").equalTo(result.user.email).once("value",function(snapshot){
                snapshot.forEach(function(childsnapshot){
                    if (childsnapshot.val().email == result.user.email)
                    {
                        window.location.replace("chatroom.html");
                        flag = true;
                    }
                })
            })
            if (!flag)
            {
                google_signin_user = result.user;
                $("#username_input").css("display","inline-block");
                $("#username_label").css("display","inline-block");
                $("#email_label").hide();
                $("#password_label").hide();
                $("#email_input").hide();
                $("#sign_in_btn").hide();
                $("#password_input").hide();
                $("#sign_in_with_google_btn").hide();
                $("#sign_up_btn").html("Submit");
                google_signin = true;
            }
        }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            var email = error.email;
            var credential = error.credential;
            $("#notification_word").html(errorMessage);
            $("#notification").toast("show");
        });
    })
}