# Software Studio 2020 Spring Midterm Project

## Topic
* Project Name : Chatroom
* Key functions
    1. Private chat room
    2. Public chat room
    
* Other functions
    1. Unsend
    2. Browser notification
    3. Third-party sign in

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Public Room||Y|
|Unsend Message||Y|

## Website Detail Description
![](https://i.imgur.com/0pmUATn.png)
進入網址後，會出現登入頁面，必須先登入才能使用聊天室功能。
可使用email登入或是google account登入。
登入後網頁會轉跳至聊天室頁面。
<br>

![](https://i.imgur.com/oJcnMHT.png)
在聊天室主頁中，左上角是目前登入的帳號，"Rooms"按鈕會把畫面切換至房間列表，"Public Room"按鈕會把畫面切換至public chatroom，"Logout"按鈕會將帳號登出並且將畫面轉跳至登入畫面。
<br>

![](https://i.imgur.com/tat2lGg.png)

在聊天室畫面中，左下角的input是文字input，右下角的是邀請input，輸入email即可邀請，若輸入的email不存在，則無法邀請。
中間的是文字部分，在右邊的是自己發送的文字，左邊的是對方發送的文字，html code已經過處理。
自己發送的文字可以透過叉叉按鈕收回，所收回的訊息會在對方的畫面即時消失。
![](https://i.imgur.com/QBgvlFn.png)
發送文字後，對方會跳出browser notification。
<br>

![](https://i.imgur.com/8SQ1WTz.png)
public room跟private room差不多，唯一不同的是每個人都可以發言。


# 作品網址：https://midterm-project-10706233-37c1c.web.app/

# Components Description : 
## Sign in
```
$("#sign_in_btn").click(function()
{   
    let email = $("#email_input").val();
    let password = $("#password_input").val();
    firebase.auth().signInWithEmailAndPassword(email, password).then(function() {
        window.location.replace("chatroom.html");
    }).catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        $("#notification_word").html(errorMessage);
        $("#notification").toast("show");
    });   
})
```
按sign in鍵時使用email登入並切換頁面。
```
$("#sign_up_btn").click(function(){
        if (signing_up)
        {
            var _username = $("#username_input").val();
            var _email = $("#email_input").val();
            var password = $("#password_input").val();
            firebase.database().ref("users/").once("value",function(snapshot){
                if (snapshot.child(_username).exists())
                {
                    $("#notification_word").html("The username is already used.");
                    $("#notification").toast("show");
                }
                else
                {
                    firebase.auth().createUserWithEmailAndPassword(_email, password).then(function(){
                        $("#notification_word").html("Sign up success");
                        $("#notification img").attr("src","icons/done_icon.png");
                        $("#notification").toast("show");
                        setTimeout(()=>{$("#notification img").attr("src","icons/wrong_icon.png");},1200);
                        signing_up = false;
                        $("#username_input").hide();
                        $("#username_label").hide();
                        $("#cancel_sign_up_btn").hide();
                        $("#sign_in_btn").show();
                        $("#sign_in_with_google_btn").show();
                        firebase.database().ref("users/"+_username).set({
                        email:_email,
                        photoURL:""
                        })  
                    }).catch(function(error) {
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        $("#notification_word").html(errorMessage);
                        $("#notification").toast("show");
                    });
                    
                }
            })
        }
        else if (google_signin)
        {   
            var _username = $("#username_input").val();
            firebase.database().ref("users/").once("value",function(snapshot){
                if (snapshot.child(_username).exists())
                {
                    $("#notification_word").html("The username is already used.");
                    $("#notification").toast("show");
                }
                else
                {
                    firebase.database().ref("users/"+_username).set({
                        email:google_signin_user.email,
                        uid:google_signin_user.uid,
                        photoURL:""
                    })
                    $("#username_input").val("");
                    $("#sign_up_btn").html("Sign up");
                    $("#username_input").hide();
                    $("#username_label").hide();
                    $("#email_label").show();
                    $("#password_label").show();
                    $("#email_input").show();
                    $("#sign_in_btn").show();
                    $("#password_input").show();
                    $("#sign_in_with_google_btn").show();
                    window.location.replace("chatroom.html");
                }
            })
        }
        else
        {
            signing_up = true;
            $("#username_input").css("display","inline-block");
            $("#username_label").css("display","inline-block");
            $("#cancel_sign_up_btn").show();
            $("#sign_in_btn").hide();
            $("#sign_in_with_google_btn").hide();
            
        }
    })
```
當按下sign up時，會先將畫面切換至註冊畫面。
當按下第二次sign up時，會先檢查username是否有被註冊過，若沒有被註冊過，則會將資料push到database上，若有被註冊過，則會跳出警告並且不會做任何動作。
```
$("#sign_in_with_google_btn").click(function(){
    let google_provider = new firebase.auth.GoogleAuthProvider();
    let flag = false;
    firebase.auth().signInWithPopup(google_provider).then(function(result) 
    {
        var token = result.credential.accessToken;
        var user = result.user;
        firebase.database().ref("users/").orderByChild("email").equalTo(result.user.email).once("value",function(snapshot){
            snapshot.forEach(function(childsnapshot){
                if (childsnapshot.val().email == result.user.email)
                {
                    window.location.replace("chatroom.html");
                    flag = true;
                }
            })
        })
        if (!flag)
        {
            google_signin_user = result.user;
            $("#username_input").css("display","inline-block");
            $("#username_label").css("display","inline-block");
            $("#email_label").hide();
            $("#password_label").hide();
            $("#email_input").hide();
            $("#sign_in_btn").hide();
            $("#password_input").hide();
            $("#sign_in_with_google_btn").hide();
            $("#sign_up_btn").html("Submit");
            google_signin = true;
        }
    }).catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        var email = error.email;
        var credential = error.credential;
        $("#notification_word").html(errorMessage);
        $("#notification").toast("show");
    });
})
}
```
若使用google登入，第一次時會要求輸入username，若username存在，則會跳警告並不會做任何事。
若username不存在，則會自動登入。

## Chatroom
```
this.Notification.requestPermission(function(state){
        if (this.Notification.permission !== status)
            this.Notification.permission = status;
    })
```
進入網頁後，會先要求通知存取權限。
```
firebase.auth().onAuthStateChanged(function(user){
        if (user)
        {
            $("#notification_word").html("Login success");
            $("#notification").toast("show");
            $("#nav_bar_account").html(user.email);
            user_email = user.email;
            firebase.database().ref("users").orderByChild("email").equalTo(user_email).once("value",function(snapshot){
                    snapshot.forEach(function(childsnapshot){
                        user_name = childsnapshot.ref.key;
                })
            })
        }
        else
        {
            window.location.replace("index.html");
        }
        
    })
```
登入後會更新使用者資料，並將email跟username存在變數裡。
若進入到網頁裡沒有user，則會把網頁轉跳至登入畫面。

```
$("#room_btn").click(function(){
        $("#main").show(500);
        $("#chatroom").hide(500);
        $("#p_chatroom").hide(500);
        setTimeout(function(){$("#text_part").html("");$("#p_text_part").html("");},500);
        firebase.database().ref("rooms/"+current_room_ref+'/text').off();
        firebase.database().ref('public'+'/text').off();
        current_room_ref = "";
    })
```
如果按navbar的"Room"，會把畫面切換至private room列表。
```
$("#p_room_btn").click(function(){
        if (current_room_ref!='public')
        {
            $("#main").hide(500);
            $("#chatroom").hide(500);
            $("#p_chatroom").show(500);
            firebase.database().ref("rooms/"+current_room_ref+'/text').off();
            current_room_ref = 'public';
            firebase.database().ref('public/text').on("child_added",function(ssnapshot)
            {   
                if (ssnapshot.val().sender != user_email)
                {
                    $("#p_text_part").append('<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false" id='+ssnapshot.key+'><div class="toast-header"><img src="icons/message_icon.png" class="rounded mr-2"></img><strong class="mr-auto"></strong></div><div class="toast-body" style="word-wrap:break-word;"></div></div>');
                }
                else
                {
                    $("#p_text_part").append('<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false" style="text-align:right;" id='+ssnapshot.key+'><div class="toast-header" style="justify-content:flex-end;"><img src="icons/message_icon.png" class="rounded mr-2"></img><strong class="mr-auto" style="margin-right:0 !important"></strong><button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="toast-body" style="word-wrap:break-word;"></div></div>');
                }
                $("#"+ssnapshot.key+' strong').text(ssnapshot.val().username);
                $("#"+ssnapshot.key+' .toast-body').text(ssnapshot.val().text);
                $("#"+ssnapshot.key+' button').click(function(){
                    ssnapshot.ref.remove();
                    setTimeout(()=>{$("#"+ssnapshot.key).remove()},500);
                })
                $("#p_text_part .toast").toast("show");
                p_text_part.scrollTop = p_text_part.scrollHeight;
            })
            firebase.database().ref("public/text").on("child_removed",function(ssnapshot)
            {   
                if (ssnapshot.val().sender!=user_email)
                    $("#"+ssnapshot.key).remove();
            })
        }
    })
```
按下"Public Room"則會顯示public room。
Onclickfunction所做的事為將畫面切換至private room、判斷是否有invite過、為傳送訊息增加listener、顯示過往訊息、為收回訊息增加listener。
```
$("#logout_btn").click(function(){
        firebase.auth().signOut().then(function(){
            $("#text_part").html("");
            $("#p_text_part").html("");
            firebase.database().ref("rooms/"+current_room_ref+'/text').off();
            firebase.database().ref('public'+'/text').off();
            window.location.replace("index.html");
        }).catch(function(error){
            alert(error.message);
        })
        $("#text_part").html("");
    })
```
按下logout會使用firebase的logout function並切換畫面。
這個function也會把某些listener關掉。
```
$("#add_room_btn").click(function(){
        var chatroom_name = $("#add_room_input").val();
        if (chatroom_name != "")
        {
            firebase.database().ref("rooms/").push({
            name:chatroom_name,
            establisher:user_email,
            receiver:"",
            timestamp:+ new Date()
            })
            $("#notification_word").html("Room "+ chatroom_name +" added");
            $("#notification").toast("show");
        }
        else
        {
            $("#notification_word").html("Room name can not be empty");
            $("#notification img").attr("src","icons/wrong_icon.png");
            $("#notification").toast("show");
            setTimeout(()=>{$("#notification img").attr("src","icons/done_icon.png");},1200);
            
        } 
    })
```
按下"Add Room"按鈕會將房間資訊push至database裡，並且顯示通知。
房間名字不能為空。
```
firebase.database().ref("rooms/").on("child_added",function(snapshot){
        if (snapshot.val().establisher == user_email || snapshot.val().receiver == user_email)
        {   
            $("#room_list").append('<button class="btn btn-primary" id='+ snapshot.key + '>' + snapshot.val().name +'</button>');
            associated_rooms.push(snapshot.key);
            $("#"+snapshot.key).click(function(){
                $("#main").hide(500);
                $("#chatroom").show(500);
                current_room_ref = this.id;
                firebase.database().ref("rooms/"+current_room_ref).once("value",function(ssnapshot){
                    if(ssnapshot.val().receiver != "") $("#invite_input").attr("readonly","readonly")
                    else $("#invite_input").removeAttr("readonly");
                })
                firebase.database().ref("rooms/"+current_room_ref+'/text').on("child_added",function(ssnapshot)
                {   
                    if (ssnapshot.val().sender != user_email)
                    {   
                        
                        $("#text_part").append('<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false" id='+ssnapshot.key+'><div class="toast-header"><img src="icons/message_icon.png" class="rounded mr-2"></img><strong class="mr-auto"></strong></div><div class="toast-body" style="word-wrap:break-word;"></div></div>');
                    }
                    else
                    {
                        $("#text_part").append('<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false" style="text-align:right;" id='+ssnapshot.key+'><div class="toast-header" style="justify-content:flex-end;"><img src="icons/message_icon.png" class="rounded mr-2"></img><strong class="mr-auto" style="margin-right:0 !important"></strong><button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="toast-body" style="word-wrap:break-word;"></div></div>');
                    }
                    $("#"+ssnapshot.key+' strong').text(ssnapshot.val().username);
                    $("#"+ssnapshot.key+' .toast-body').text(ssnapshot.val().text);
                    $("#"+ssnapshot.key+' button').click(function(){
                        ssnapshot.ref.remove();
                        setTimeout(()=>{$("#"+ssnapshot.key).remove()},500);
                    })
                    $("#text_part .toast").toast("show");
                    text_part.scrollTop = text_part.scrollHeight;
                }) 
                firebase.database().ref("rooms/"+current_room_ref+'/text').on("child_removed",function(ssnapshot)
                {   
                    if (ssnapshot.val().sender!=user_email)
                        $("#"+ssnapshot.key).remove();
                })
            })
        }
    })
```
當database裡有房間增加時，room list的button會增加，並且每次都會為當前所增加的button增加onclickfunction。
Onclickfunction所做的事為將畫面切換至private room、判斷是否有invite過、為傳送訊息增加listener、顯示過往訊息、為收回訊息增加listener。
```
firebase.database().ref("rooms/").on("child_changed",function(snapshot){
        if (snapshot.val().receiver == user_email)
        {   
            if (associated_rooms.findIndex(function(key){return key==snapshot.key;}) == -1)
            {   
                $("#room_list").append('<button class="btn btn-primary" id='+ snapshot.key + '>'+ snapshot.val().name +'</button>');
                associated_rooms.push(snapshot.key);
                $("#"+snapshot.key).click(function(){
                    $("#main").hide();
                    $("#chatroom").show();
                    current_room_ref = this.id;
                    firebase.database().ref("rooms/"+current_room_ref).once("value",function(ssnapshot){
                        if(ssnapshot.val().receiver != "") $("#invite_input").attr("readonly","readonly")
                        else $("#invite_input").removeAttr("readonly");
                    })
                    firebase.database().ref("rooms/"+current_room_ref+'/text').on("child_added",function(ssnapshot)
                    {
                        if (ssnapshot.val().sender != user_email)
                        {
                            $("#text_part").append('<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false" id='+ssnapshot.key+'><div class="toast-header"><img src="icons/message_icon.png" class="rounded mr-2"></img><strong class="mr-auto"></strong></div><div class="toast-body" style="word-wrap:break-word;"></div></div>');
                        }
                        else
                        {
                            $("#text_part").append('<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false" style="text-align:right;" id='+ssnapshot.key+'><div class="toast-header" style="justify-content:flex-end;"><img src="icons/message_icon.png" class="rounded mr-2"></img><strong class="mr-auto" style="margin-right:0 !important"></strong><button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="toast-body" style="word-wrap:break-word;"></div></div>');
                        }
                        $("#"+ssnapshot.key+' strong').text(ssnapshot.val().username);
                        $("#"+ssnapshot.key+' .toast-body').text(ssnapshot.val().text);
                        $("#"+ssnapshot.key+' button').click(function(){
                            ssnapshot.ref.remove();
                            setTimeout(()=>{$("#"+ssnapshot.key).remove()},500);
                        })
                        $("#text_part .toast").toast("show");
                        text_part.scrollTop = text_part.scrollHeight;
                    })
                    firebase.database().ref("rooms/"+current_room_ref+'/text').on("child_removed",function(ssnapshot)
                    {   
                        if (ssnapshot.val().sender!=user_email)
                            $("#"+ssnapshot.key).remove();
                    })    
                })
            }
        }
    })
```
當輸入受邀者的email時，會用child_changed判斷room是否有改動，並且用一個array判斷這個改動是不是receiver的改動。
這個function也會即時更新receiver的room list。
其他做的事情跟child_added的function一樣。
```
$("#send_btn").click(function(){
        firebase.database().ref("rooms/" + current_room_ref + '/text/' + (+new Date())).set(
        {
            text:$("#text_input").val(),
            sender:user_email,
            username:user_name
        })
        $("#text_input").val("");
    })

    $("#p_send_btn").click(function(){
        firebase.database().ref("public" + '/text/' + (+new Date())).set(
        {
            text:$("#p_text_input").val(),
            sender:user_email,
            username:user_name
        })
        $("#p_text_input").val("");
    })
```
當按下private room或public room的send時，會把訊息push至database裡，key為當前的timestamp。
```
$("#invite_btn").click(function(){
        if ($("#invite_input").val() == user_email)
        {
            $("#notification_word").html("You can't invite youself");
            $("#notification img").attr("src","icons/wrong_icon.png");
            $("#notification").toast("show");
            setTimeout(()=>{$("#notification img").attr("src","icons/done_icon.png");},1200);
        }
        else if ($("#invite_input").val().search(/^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/)==-1)
        {
            $("#notification_word").html("Please enter right email");
            $("#notification img").attr("src","icons/wrong_icon.png");
            $("#notification").toast("show");
            setTimeout(()=>{$("#notification img").attr("src","icons/done_icon.png");},1200);
        }
        else
        {   
            var flag = false;
            firebase.database().ref("rooms/users").once("value",function(user_snapshot){
                user_snapshot.forEach(function(childsnapshot){
                    if (childsnapshot.val().email == $("#invite_input").val())
                    {
                        firebase.database().ref("rooms/" + current_room_ref).update({receiver:$("#invite_input").val()}).then(function(){
                            $("#invite_input").val("");
                            $("#invite_input").attr("readonly","readonly");
                            $("#notification_word").html("Invite success");
                            $("#notification").toast("show");
                        }).catch(function(error){
                            $("#notification_word").html(error.message);
                            $("#notification img").attr("src","icons/wrong_icon.png");
                            $("#notification").toast("show");
                            setTimeout(()=>{$("#notification img").attr("src","icons/done_icon.png");},1200);
                        });
                        $("#invite_input").html("");
                        flag = true;
                    }
                })
            })
            if (!flag)
            {
                $("#notification_word").html("User doesn't exist.");
                $("#notification img").attr("src","icons/wrong_icon.png");
                $("#notification").toast("show");
                setTimeout(()=>{$("#notification img").attr("src","icons/done_icon.png");},1200);
            }
            
        }
    })
```
按下private room的invite button時，會先判斷是不是邀請自己，如果是，會顯示錯誤。
接下來會判斷輸入的是不是email，如果不是，會顯示錯誤。
如果前兩個if都通過了，會先判斷email是否存在，如果不存在會跳出警告，如果存在，會把database裡房間的receiver改成input裡的email。
# Other Functions Description : 
## Browser notification
```
firebase.database().ref("rooms/").on("child_added",function(snapshot){
        snapshot.child("text").ref.orderByKey().startAt((+ new Date()).toString()).on("child_added",function(text_data){
            if (snapshot.val().receiver == user_email && Notification && Notification.permission == "granted")
            {   
                var notif = new Notification("New message from " + text_data.val().username + " in Room '" + snapshot.val().name + "'" , {body:text_data.val().text});
                setTimeout(function(){notif.close()},2000);
            }
        })
    })

    firebase.database().ref("public/text").orderByKey().startAt((+ new Date()).toString()).on("child_added",function(text_data){
        if (snapshot.val().receiver == user_email && Notification && Notification.permission == "granted")
        {   
            var notif = new Notification("New message from " + text_data.val().username + " in Public Room ", {body:text_data.val().text});
            setTimeout(function(){notif.close()},2000);
        }
    })
```
上面的是private room的listener，下面的是public room的listener。
private room的第一層listener是存取每一個room，並且在每次增加room時都會加上訊息的listener。
第二層則是訊息的listener。
public room只需訊息的listener而已。
## Unsend
```
$("#"+ssnapshot.key+' button').click(function(){
    ssnapshot.ref.remove();
    setTimeout(()=>{$("#"+ssnapshot.key).remove()},500);
})
firebase.database().ref("rooms/"+current_room_ref+'/text').on("child_removed",function(ssnapshot)
{   
    if (ssnapshot.val().sender!=user_email)
    $("#"+ssnapshot.key).remove();
})  
```
每個自己發送的message左上角都有個叉叉的button，按下去後會觸發firebase的remove。
觸發後，會再觸發child_removed的listener，把訊息移除。
